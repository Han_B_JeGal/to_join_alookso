class HomeController < ApplicationController
  def index
    @articles = Article.order('id DESC').page params[:page]

  end

  def writing
  end

  def send_article_to_db
    article = Article.new
    article.subject = params[:subject]
    article.content = params[:content]
    article.save

    redirect_to '/'
  end

  def show
    @articles = Article.find_by(id: params[:id])
  end
end
