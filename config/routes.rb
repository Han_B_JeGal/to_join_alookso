Rails.application.routes.draw do
  get '/' => 'home#index'

  get '/writing' => 'home#writing'

  post '/send_article_to_db' => 'home#send_article_to_db'

  get '/show', to: 'home#show'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#writing"
end
